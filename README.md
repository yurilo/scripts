##This folder contains usefull scripts.

To add scripts to your PATH you should add all folders to the PATH variable.

Add next string to your .profile file.
NOTE: suppose that the scripts folder is in you home directory.

PATH=${PATH}:$(find ~/scripts -type d -not \( -name .svn -prune \) | tr '\n' ':' | sed 's/:$//')
