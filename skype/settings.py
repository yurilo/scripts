#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
SKYPE_FOLDER = "~/.Skype/anischenko.yuri.aleksandrovich"
CONTACTS_NOTIFY_LIST = "~/.Skype/anischenko.yuri.aleksandrovich/filter_notify.conf"
 
COMMENT = "#"
DELIMITER = "#"
 
ONLINE_MESSAGE = "снова в сети"
 
 
import re
regex = re.compile("(" + COMMENT + "*)([^#]+)" + DELIMITER + "([^\n]+)")
 
# Статус оповещения для новых добавляемых контактов. Может быть COMMENT
# (закомментированы, т.е. не оповещать) или "" (раскомментированы - оповещать).
# В случае установки параметра DEFAULT_COMMENT = "" оповещение будет происходить
# только после первого запуска reload_contacts.py. В этом случае в скрипт
# filter_notify.py после запуска reload_contacts.py неплохо бы добавить новую
# проверку существования target_skype_name в новом списке контактов.
DEFAULT_COMMENT = COMMENT
